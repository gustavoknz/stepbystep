package com.kieling.stepbystep.util;

/*
 * Auxiliary class
 */
public final class Constants {
    public static final String ENTRY_LIST_PARAMETER_KEY = "isLocationActive";
    public static final String PATH_PARAMETER_ID_KEY = "journeyId";
    public static final String PATH_PARAMETER_START_KEY = "startTime";
    public static final String PATH_PARAMETER_END_KEY = "endTime";

    public static final String SERVICE_NEW_LOCATION_RESULT = "locationResult";
    public static final String SERVICE_NEW_LOCATION_LATITUDE = "locationLatitude";
    public static final String SERVICE_NEW_LOCATION_LONGITUDE = "locationLongitude";
}
