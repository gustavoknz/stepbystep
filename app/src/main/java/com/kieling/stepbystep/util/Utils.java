package com.kieling.stepbystep.util;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.kieling.stepbystep.model.MapEntry;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class Utils {
    private static final String TAG = "Utils";
    private static long mCurrentJourneyId;

    static {
        List<MapEntry> result = MapEntry.find(MapEntry.class, null, null, null, "journey_id DESC", "1");
        Log.d(TAG, "Searching for the MAX(journeyId): " + result);
        mCurrentJourneyId = result.size() <= 0 ? 1 : result.get(0).getJourneyId() + 1;
    }

    public static void addPoint(double latitude, double longitude) {
        Log.d(TAG, String.format("Saving entry journeyId: %d; (%f, %f)", mCurrentJourneyId, latitude, longitude));
        new MapEntry(mCurrentJourneyId, new Date().getTime(), latitude, longitude).save();
    }

    public static void resetJourney() {
        ++mCurrentJourneyId;
    }

    public static List<LatLng> getAllPoints() {
        Log.d(TAG, "Looking for id " + Long.toString(Math.max(mCurrentJourneyId - 1, 0)));
        List<MapEntry> result = MapEntry.find(MapEntry.class, "journey_id = ?", Long.toString(Math.max(mCurrentJourneyId - 1, 0)));
        List<LatLng> points = new ArrayList<>();
        for (MapEntry entry : result) {
            points.add(new LatLng(entry.getLatitude(), entry.getLongitude()));
        }
        return points;
    }
}
