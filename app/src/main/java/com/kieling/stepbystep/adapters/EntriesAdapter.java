package com.kieling.stepbystep.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kieling.stepbystep.R;
import com.kieling.stepbystep.activities.PathActivity;
import com.kieling.stepbystep.model.MapEntryToShow;
import com.kieling.stepbystep.util.Constants;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Holds all items in the list
 */
public class EntriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final Locale LOCALE = Locale.getDefault();
    private static final DateFormat DATE_FORMAT = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, LOCALE);
    private List<MapEntryToShow> mDataSet;
    private Context mContext;

    public EntriesAdapter(List<MapEntryToShow> data, Context context) {
        this.mDataSet = data;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.entry_item, parent, false);
        return new EntryViewHolder(view, mContext);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int listPosition) {
        MapEntryToShow entry = mDataSet.get(listPosition);
        EntryViewHolder viewHolder = (EntryViewHolder) holder;
        viewHolder.idView.setText(String.format(LOCALE, "%d", entry.getJourneyId()));
        viewHolder.startDateView.setText(DATE_FORMAT.format(new Date(entry.getStartTime())));
        if (entry.getEndTime() == 0) {
            viewHolder.endDateView.setText(R.string.entry_item_in_progress);
        } else {
            viewHolder.endDateView.setText(DATE_FORMAT.format(new Date(entry.getEndTime())));
        }
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    static class EntryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private static final String TAG = "EntryViewHolder";

        @BindView(R.id.itemId)
        TextView idView;
        @BindView(R.id.itemStartDate)
        TextView startDateView;
        @BindView(R.id.itemEndDate)
        TextView endDateView;
        @BindView(R.id.entryItemCardView)
        CardView cardView;
        private Context context;

        EntryViewHolder(View itemView, Context context) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.cardView.setOnClickListener(this);
            this.context = context;
        }

        @Override
        public void onClick(View view) {
            try {
                int journeyId = Integer.parseInt(((TextView) view.findViewById(R.id.itemId)).getText().toString());
                String startDate = ((TextView) view.findViewById(R.id.itemStartDate)).getText().toString();
                String endDate = ((TextView) view.findViewById(R.id.itemEndDate)).getText().toString();
                Log.d(TAG, "Parameter journeyId = " + journeyId);
                Intent intent = new Intent(context, PathActivity.class);
                intent.putExtra(Constants.PATH_PARAMETER_ID_KEY, journeyId);
                intent.putExtra(Constants.PATH_PARAMETER_START_KEY, startDate);
                intent.putExtra(Constants.PATH_PARAMETER_END_KEY, endDate);
                context.startActivity(intent);
            } catch (Exception e) {
                Log.e(TAG, "Error loading is from item", e);
            }
        }
    }
}
