package com.kieling.stepbystep.model;

/*
 * Contains all data to be shown in the list (see {@link com.kieling.stepbystep.activities.EntryListActivity})
 */
public class MapEntryToShow {
    private long journeyId;
    private long startTime;
    private long endTime;

    public long getJourneyId() {
        return journeyId;
    }

    public void setJourneyId(long journeyId) {
        this.journeyId = journeyId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }
}
