package com.kieling.stepbystep.model;

import com.orm.SugarRecord;

/*
 * Reflect database table
 */
public class MapEntry extends SugarRecord<MapEntry> {
    private long journeyId;
    private long currentTime;
    private double latitude;
    private double longitude;

    //Constructor required by Sugar
    public MapEntry() {
    }

    public MapEntry(long journeyId, long currentTime, double latitude, double longitude) {
        this.journeyId = journeyId;
        this.currentTime = currentTime;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public long getJourneyId() {
        return journeyId;
    }

    public void setJourneyId(long journeyId) {
        this.journeyId = journeyId;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "MapEntry{" +
                "id=" + id +
                ", journeyId=" + journeyId +
                ", currentTime=" + currentTime +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
