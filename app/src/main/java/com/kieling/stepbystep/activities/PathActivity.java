package com.kieling.stepbystep.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.kieling.stepbystep.R;
import com.kieling.stepbystep.model.MapEntry;
import com.kieling.stepbystep.util.Constants;

import java.util.List;

/*
 * See in the map all route points
 */
public class PathActivity extends FragmentActivity implements OnMapReadyCallback {
    private static final String TAG = "PathActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_path);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.pathMap);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        int journeyId = getIntent().getIntExtra(Constants.PATH_PARAMETER_ID_KEY, -1);
        String startTime = getIntent().getStringExtra(Constants.PATH_PARAMETER_START_KEY);
        String endTime = getIntent().getStringExtra(Constants.PATH_PARAMETER_END_KEY);
        Log.d(TAG, "Looking for journeyId = " + journeyId);
        Log.d(TAG, String.format("startTime: %s; endTime: %s", startTime, endTime));
        List<MapEntry> points = MapEntry.find(MapEntry.class, "journey_id = ?", Integer.toString(journeyId));
        Log.d(TAG, "Result for all journeyId (" + journeyId + "): " + points.size());

        final LatLngBounds.Builder builder = new LatLngBounds.Builder();
        LatLng currentLatLng;
        if (points.size() > 0) {
            currentLatLng = new LatLng(points.get(0).getLatitude(), points.get(0).getLongitude());
            googleMap.addMarker(new MarkerOptions()
                    .position(currentLatLng)
                    .title(getString(R.string.path_initial_position_text, startTime))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
            builder.include(currentLatLng);
        }
        if (points.size() > 1) {
            for (int i = 0; i < points.size() - 1; i++) {
                currentLatLng = new LatLng(points.get(i).getLatitude(), points.get(i).getLongitude());
                googleMap.addPolyline(new PolylineOptions()
                        .add(currentLatLng)
                        .add(new LatLng(points.get(i + 1).getLatitude(), points.get(i + 1).getLongitude()))
                        .width(10)
                        .color(Color.GREEN)
                        .geodesic(true));
                builder.include(currentLatLng);
            }
            currentLatLng = new LatLng(points.get(points.size() - 1).getLatitude(), points.get(points.size() - 1).getLongitude());
            googleMap.addMarker(new MarkerOptions()
                    .position(currentLatLng)
                    .title(getString(R.string.path_last_position_text, startTime))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
            builder.include(currentLatLng);
        }
        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = Math.round(Math.min(width, height) * 0.15F);
        try {
            //Offset from edges of the map 15% of screen
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
            googleMap.animateCamera(cu);
        } catch (Exception e) {
            Log.e(TAG, "Error when moving camera", e);
        }
    }
}
