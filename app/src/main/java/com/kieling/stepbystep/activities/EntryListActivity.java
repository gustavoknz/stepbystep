package com.kieling.stepbystep.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.kieling.stepbystep.R;
import com.kieling.stepbystep.adapters.EntriesAdapter;
import com.kieling.stepbystep.model.MapEntry;
import com.kieling.stepbystep.model.MapEntryToShow;
import com.kieling.stepbystep.util.Constants;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Activity where user can check out all entries (paths)
 */
public class EntryListActivity extends AppCompatActivity {
    private static final String TAG = "EntryListActivity";

    @BindView(R.id.entryListRecyclerView)
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_list);
        ButterKnife.bind(this);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        boolean isActive = getIntent().getBooleanExtra(Constants.ENTRY_LIST_PARAMETER_KEY, false);
        Log.d(TAG, "received isActive = " + isActive);

        final List<MapEntryToShow> listAdapter = new ArrayList<>();
        MapEntry mapEntry;
        String query = "SELECT *" +
                " FROM map_entry me1" +
                " WHERE id = (SELECT MAX(id) FROM map_entry me2 WHERE me1.journey_id = me2.journey_id)" +
                "    OR id = (SELECT MIN(id) FROM map_entry me2 WHERE me1.journey_id = me2.journey_id)" +
                " ORDER BY journey_id, id;";
        Iterator<MapEntry> iterator = MapEntry.findWithQueryAsIterator(MapEntry.class, query);
        int j = 0;
        List<MapEntry> entries = new ArrayList<>();
        while (iterator.hasNext()) {
            mapEntry = iterator.next();
            Log.d(TAG, String.format("(filter) Entry #%d: %s", j++, mapEntry));
            entries.add(mapEntry);
        }
        MapEntry thisEntry;
        MapEntryToShow entryToShow;
        for (int i = 0; i < entries.size(); i++) {
            thisEntry = entries.get(i);
            entryToShow = new MapEntryToShow();
            entryToShow.setJourneyId(thisEntry.getJourneyId());
            if (entries.size() <= i + 1) {
                //Reached the last element, which does not have a pair
                //Still may be active generating entries
                entryToShow.setStartTime(thisEntry.getCurrentTime());
                if (isActive) {
                    Log.d(TAG, "Still active");
                } else {
                    entryToShow.setEndTime(thisEntry.getCurrentTime());
                }
            } else if (thisEntry.getJourneyId() == entries.get(i + 1).getJourneyId()) {
                //It has a pair
                entryToShow.setStartTime(thisEntry.getCurrentTime());

                //The pair was the end if this element is the last but one AND tracking is off
                if (entries.size() == i + 2 && isActive) {
                    Log.d(TAG, "Still active");
                } else {
                    entryToShow.setEndTime(entries.get(i + 1).getCurrentTime());
                }

                //Jump one element (the pair)
                ++i;
            } else {
                //It started and did not get any other record
                entryToShow.setStartTime(thisEntry.getCurrentTime());
                entryToShow.setEndTime(thisEntry.getCurrentTime());
            }
            listAdapter.add(entryToShow);
        }

        Log.d(TAG, "Adding items to list: " + listAdapter.size());
        EntriesAdapter adapter = new EntriesAdapter(listAdapter, getApplicationContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);

        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        //Use finish or set PARENT_ACTIVITY in manifest
        finish();
        return true;
    }
}
