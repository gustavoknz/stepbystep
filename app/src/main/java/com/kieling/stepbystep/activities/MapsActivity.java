package com.kieling.stepbystep.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.fastaccess.permission.base.PermissionHelper;
import com.fastaccess.permission.base.callback.OnPermissionCallback;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.kieling.stepbystep.R;
import com.kieling.stepbystep.service.StepService;
import com.kieling.stepbystep.util.Constants;
import com.kieling.stepbystep.util.Utils;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Main activity, where user see his/her movement in map
 */
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        OnPermissionCallback, //LocationListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "MapsActivity";
    private static final String[] MULTI_PERMISSIONS = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};

    @BindView(R.id.mainTrackingButton)
    ToggleButton trackingButton;

    private boolean mFirstPosition = true;
    private GoogleMap mMap;
    private PermissionHelper mPermissionHelper;
    private AlertDialog mAlertDialog;
    private LatLng mPreviousLocation;
    private GoogleApiClient mGoogleApiClient;
    private BroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Log.d(TAG, "I am already connected!");
        } else {
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.mainMap);
            mapFragment.getMapAsync(this);
        }
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                double latitude = intent.getDoubleExtra(Constants.SERVICE_NEW_LOCATION_LATITUDE, 0);
                double longitude = intent.getDoubleExtra(Constants.SERVICE_NEW_LOCATION_LONGITUDE, 0);
                Log.d(TAG, "Received new location: " + latitude + "; " + longitude);

                //Place only initial location marker
                LatLng latLng = new LatLng(latitude, longitude);
                markPositionOnMap(latLng);

                //move map camera
                //TODO use LatLngBounds.Builder, including all positions in a specific zoom
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18f));
            }
        };
    }

    private void markPositionOnMap(LatLng latLng) {
        if (mFirstPosition) {
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(latLng)
                    .title(getString(R.string.main_initial_position_text))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
            mMap.addMarker(markerOptions);
            mFirstPosition = false;
        } else {
            drawLine(latLng);
        }
        mPreviousLocation = latLng;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Ask for permission
        mPermissionHelper = PermissionHelper.getInstance(this);
        mPermissionHelper
                .setForceAccepting(false) //true if you had like force reshowing the permission dialog on Deny (not recommended)
                .request(MULTI_PERMISSIONS);

        List<LatLng> points = Utils.getAllPoints();
        Log.d(TAG, "Got initial points: " + points);
        for (LatLng point : points) {
            markPositionOnMap(point);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: registering receiver");
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver), new IntentFilter(Constants.SERVICE_NEW_LOCATION_RESULT));
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop: unregistering receiver");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onStop();
    }

    //region Permissions checker
    //OnPermissionCallback
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i(TAG, "(onRequestPermissionsResult) grantResults.length: " + grantResults.length);
        mPermissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    //OnPermissionCallback
    @Override
    public void onPermissionGranted(@NonNull String[] permissionName) {
        Log.i(TAG, "Permission(s) " + Arrays.toString(permissionName) + " Granted");

        //Mandatory check -.-
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);

        if (trackingButton.isChecked()) {
            Log.d(TAG, "Setting info for the first time");
            buildGoogleApiClient();
        } else {
            Log.d(TAG, "Tracking button not checked");
        }
    }

    //OnPermissionCallback
    @Override
    public void onPermissionDeclined(@NonNull String[] permissionName) {
        Log.i(TAG, "Permission(s) " + Arrays.toString(permissionName) + " Declined");
    }

    //OnPermissionCallback
    @Override
    public void onPermissionPreGranted(@NonNull String permissionsName) {
        Log.i(TAG, "Permission (" + permissionsName + ") preGranted");
    }

    //OnPermissionCallback
    @Override
    public void onPermissionNeedExplanation(@NonNull String permissionName) {
        Log.i(TAG, "Permission (" + permissionName + ") needs explanation");
        String[] neededPermission = PermissionHelper.declinedPermissions(this, MULTI_PERMISSIONS);
        StringBuilder builder = new StringBuilder(neededPermission.length);
        if (neededPermission.length > 0) {
            for (String permission : neededPermission) {
                builder.append(permission).append("\n");
            }
        }
        AlertDialog alert = getAlertDialog(neededPermission, builder.toString());
        if (!alert.isShowing()) {
            alert.show();
        }
    }

    //OnPermissionCallback
    @Override
    public void onPermissionReallyDeclined(@NonNull String permissionName) {
        Log.i(TAG, "Permission " + permissionName + " can only be granted from settings screen");
        Toast.makeText(this, "Permission " + permissionName + " can only be granted from settings screen",
                Toast.LENGTH_LONG).show();
    }

    //OnPermissionCallback
    @Override
    public void onNoPermissionNeeded() {
        Log.i(TAG, "Permission(s) not needed");
    }
    //endregion

    //GoogleApiClient.ConnectionCallbacks
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "(onConnected) Location services connected");
        startService(new Intent(this, StepService.class));
    }

    //GoogleApiClient.ConnectionCallbacks
    @Override
    public void onConnectionSuspended(int i) {
        Log.w(TAG, "(onConnectionSuspended) i: " + i);
    }

    //GoogleApiClient.OnConnectionFailedListener
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.w(TAG, "(onConnectionFailed) connectionResult: " + connectionResult);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        disconnect();
    }

    private void connect() {
        Log.d(TAG, "Connecting...");
        mGoogleApiClient.connect();
    }

    private void disconnect() {
        Log.d(TAG, "Disconnecting...");

        //Clear map
        mMap.clear();
        mPreviousLocation = null;
        mFirstPosition = true;
        Utils.resetJourney();

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Log.d(TAG, "Removing connections...");
            //LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.unregisterConnectionCallbacks(this);
            mGoogleApiClient.disconnect();
        } else {
            Log.d(TAG, "GoogleApiClient is not connected yet");
        }
        stopService(new Intent(this, StepService.class));
    }

    private void drawLine(LatLng location) {
        if (mPreviousLocation == null || location == null) {
            //Should never happen, because it is not the first position
            Log.d(TAG, String.format("Cannot draw line from '%s' to '%s'", mPreviousLocation, location));
        } else {
            Log.d(TAG, String.format("Drawing line from '%s' to '%s'", mPreviousLocation, location));
            PolylineOptions line = new PolylineOptions()
                    .add(mPreviousLocation, location)
                    .width(10)
                    .color(Color.BLUE)
                    .geodesic(true);
            mMap.addPolyline(line);
        }
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        connect();
    }

    public void onToggleClicked(View view) {
        Log.d(TAG, "Toggle clicked: " + trackingButton.isChecked());
        if (trackingButton.isChecked()) {
            buildGoogleApiClient();
        } else {
            disconnect();
        }
    }

    public void onFabClicked(View view) {
        Log.d(TAG, "sending isActive = " + mGoogleApiClient.isConnected());
        Intent intent = new Intent(this, EntryListActivity.class);
        intent.putExtra(Constants.ENTRY_LIST_PARAMETER_KEY, mGoogleApiClient.isConnected());
        startActivity(intent);
    }

    private AlertDialog getAlertDialog(final String[] permissions, final String permissionName) {
        if (mAlertDialog == null) {
            mAlertDialog = new AlertDialog.Builder(this)
                    .setTitle("Permission Needs Explanation")
                    .create();
        }
        mAlertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Request", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mPermissionHelper.requestAfterExplanation(permissions);
            }
        });
        mAlertDialog.setMessage("Permissions need explanation (" + permissionName + ")");
        return mAlertDialog;
    }
}
